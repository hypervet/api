const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require('./generated/prisma-client');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const db = require('./db');

function createServer() {
  return new GraphQLServer({
    typeDefs: './schema/index.graphql',
    resolver: {
      Mutation,
      Query
    },
    resolverValidationOptions: {
      requireResolversForResolveType: false
    },
    context: {
      prisma
    },
  });
}

module.exports = createServer;
