const { prisma } = require('./generated/prisma-client');
const { GraphQLServer } = require('graphql-yoga');
const { jwt } = require('jsonwebtoken');
const { AuthenticationError } = require('apollo-server-core');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Subscription = require('./resolvers/Subscription');
const Chamber = require('./resolvers/Chamber');
const Sensor = require('./resolvers/Sensor');
const SensorReading = require('./resolvers/SensorReading');
const Patient = require('./resolvers/Patient');
const Treatment = require('./resolvers/Treatment');

const resolvers = {
  Query,
  Mutation,
  Subscription,
  Sensor,
  SensorReading,
  Patient,
  Chamber,
  Treatment,
}

const authenticateMiddleware = async(resolve, root, args, context, info ) => {
  let token;
  try {
    token = jwt.verify(context.request.get('Authorization'), 'secret');
  } catch (e) {
    return new AuthenticationError("Not Authorised");
  }
  const result = await resolve(root, args, context, info);
  return result;
}

const server = new GraphQLServer({
  typeDefs: './src/schema/index.graphql',
  resolvers,
  context: {
    prisma
  },
  middlewares: [authenticateMiddleware]
});

server.start(deets => console.log(`Server is now running on port ${deets.port}`));
