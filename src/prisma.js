const Bindings = require('prisma-binding');
const Client = require('./generated/prisma-client');
const { fragmentReplacements } = require('./resolvers');

module.exports = {
  client: new Client.Prisma({
    fragmentReplacements,
    endpoint: "http://localhost:4466/",
    debug: false
  }),
  bindings: new Bindings.Prisma({
    typeDefs: 'src/generated/prisma.graphql',
    fragmentReplacements,
    endpoint: "http://localhost:4466/",
    debug: false
  })
}