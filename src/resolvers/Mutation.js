const Mutation = {
  async startTreatment(parent, args, context, info) {
    const treatment = await context.prisma.createTreatment({
      ...args,
      chamber: {connect: {id: args.chamber}},
      operator: {connect: {id: args.operator}},
      patient: {connect: {id: args.patient}}
    }, info);

    return treatment;
  },
  async endTreatment(parent, args, context, info) {
    var endDateTime = new Date()
    const treatment = await context.prisma.updateTreatment({
      where: {
        id: args.id
      },
      data: {
        finished: endDateTime.toISOString(),
      }
    }, info);
    return treatment;
  },
  async createSensorReading(parent, args, context, info) {
    const reading = await context.prisma.createSensorReading({
      value: args.value,
      treatment: {
        connect: { id: args.treatment }
      },
      sensor: {
        connect: { id: args.sensor }
      }
    }, info);
    return reading;
  }
}

module.exports = Mutation;
