const Query = require('./Query');
const Mutation = require('./Mutation');
const Sensor = require('./Sensor');
const SensorReading = require('./SensorReading');


const resolvers = {
  Query,
  Mutation,
  Sensor,
  SensorReading,
};

module.exports = resolvers