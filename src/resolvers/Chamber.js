const Chamber = {
  async account(root, args, context) {
    const account = await context.prisma.chamber({id: root.id}).account();
    return account;
  }
}

module.exports = Chamber;