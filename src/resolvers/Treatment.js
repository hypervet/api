const Treatment = {
  async readings(root, args, context) {
    const readings = await context.prisma.treatment({id: root.id}).readings();
    return readings;
  },
  async operator(root, args, context) {
    const operator = await context.prisma.treatment({id: root.id}).operator();
    return operator;
  },
  async patient(root, args, context) {
    const patient = await context.prisma.treatment({id: root.id}).patient();
    return patient;
  },
  async chamber(root, args, context) {
    const chamber = await context.prisma.treatment({id: root.id}).chamber();
    return chamber;
  }
}

module.exports = Treatment;