const Query = {
  async patients(root, args, context) {
    const patients = await context.prisma.patients();
    return patients;
  },
  async treatment(root, args, context) {
    const treatment = await context.prisma.treatment({ ...args });
    return treatment;
  },
  async treatments(root, args, context) {
    const treatments = await context.prisma.treatments();
    return treatments;
  },
  async treatmentReadings(root, args, context) {
    const readings = await context.prisma.treatment({ ...args }).readings()
    return readings;
  }
}

module.exports = Query;