const SensorReading = {
  async sensor(root, args, context) {
    const sensor = await context.prisma.sensorReading({id: root.id}).sensor();
    return sensor
  },
  async treatment( root, args, context ) {
    const treatment = await context.prisma.sensorReading({id: root.id}).treatment();
    return treatment;
  }
}

module.exports = SensorReading;
