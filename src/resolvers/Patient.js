const Patient = {
  async account(root, args, context) {
    const account = await context.prisma.patient({id: root.id}).account();
    return account;
  }
}

module.exports = Patient;