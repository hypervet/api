const Sensor = {
  async sensorType(root, args, context) {
    const sensorType = await context.prisma.sensor({id: root.id}).sensorType();
    return sensorType;
  }
};

module.exports = Sensor;