const { PubSub } = require('graphql-subscriptions');
const pubsub = new PubSub();

const NEW_READING_ADDED = 'new_reading_added';

const Subscription = {
  liveReadings: {
    subscribe: () => pubsub.asyncIterator(NEW_READING_ADDED),
    resolve: (payload, args, context, info) => {
      console.log(payload);
      console.log(args);
      console.log(context);
      console.log(info);
      return context.prisma.$subscribe.SensorReading({
        mutation_in: ['CREATED'],
        node: {
          treatment: payload.id
        }
      })
      .node()
    }
  },

}

module.exports = Subscription;