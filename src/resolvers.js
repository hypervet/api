const { extractFragmentReplacements } = require('prisma-binding');
const { PubSub } = require('apollo-server');

const pubsub = new PubSub();
const SENSOR_READING_ADDED = 'SENSOR_READING_ADDED';

const resolvers = {
  Subscription: {
    sensorReadingAdded: {
      subscribe: () => {
        return pubsub.asyncIterator([SENSOR_READING_ADDED])
      }
    }
  },
  Query: {
    account: (_, args, context, info) => (
      context.prisma.bindings.query.account(args, info)),
    accounts: (_, args, context, info) => (
      context.prisma.bindings.query.accounts(args, info)),
    chamber: (_, args, context, info) => (
      context.prisma.bindings.query.chamber(args, info)),
    chambers: (_, args, context, info) => (
      context.prisma.bindings.query.chambers(args, info)),
    patient: (_, args, context, info) => (
      context.prisma.bindings.query.patient(args, info)),
    patients: (_, args, context, info) => (
      context.prisma.bindings.query.patients(args, info)),
    sensor: (_, args, context, info) => (
      context.prisma.bindings.query.sensor(args, info)),
    sensors: (_, args, context, info) => (
      context.prisma.bindings.query.sensors(args, info)),
    sensorReading: (_, args, context, info) => (
      context.prisma.bindings.query.sensorReading(args, info)),
    sensorReadings: (_, args, context, info) => (
      context.prisma.bindings.query.sensorReadings(args, info)),
    sensorType: (_, args, context, info) => (
      context.prisma.bindings.query.sensorType(args, info)),
    sensorTypes: (_, args, context, info) => (
      context.prisma.bindings.query.sensorTypes(args, info)),
    treatment: (_, args, context, info) => (
      context.prisma.bindings.query.treatment(args, info)),
    treatments: (_, args, context, info) => (
      context.prisma.bindings.query.treatments(args, info)),
    user: (_, args, context, info) => (
      context.prisma.bindings.query.user(args, info)),
    users: (_, args, context, info) => (
      context.prisma.bindings.query.users(args, info)),
  },
  Mutation: {
    createTreatment: (_, args, context, info) => (
      context.prisma.bindings.mutation.createTreatment(args, info)
    ),
    updateTreatment: (_, args, context, info) => (
      context.prisma.bindings.mutation.updateTreatment(args, info)
    ),
    createSensorReading: (_, args, context, info) => {
      console.log(args);
      pubsub.publish(SENSOR_READING_ADDED, { sensorReadingAdded: args })
      return context.prisma.bindings.mutation.createSensorReading(args, info)},
  }
}

module.exports = {
  resolvers,
  fragmentResplacements: extractFragmentReplacements(resolvers)
};
