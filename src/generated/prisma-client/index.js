"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "Account",
    embedded: false
  },
  {
    name: "User",
    embedded: false
  },
  {
    name: "Chamber",
    embedded: false
  },
  {
    name: "SensorType",
    embedded: false
  },
  {
    name: "Sensor",
    embedded: false
  },
  {
    name: "SensorReading",
    embedded: false
  },
  {
    name: "Patient",
    embedded: false
  },
  {
    name: "Treatment",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `http://localhost:4466`
});
exports.prisma = new exports.Prisma();
